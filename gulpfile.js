'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    dest = require('gulp-dest'),
    pug = require('gulp-pug'),
    babel = require('gulp-babel'),
    plumber = require('gulp-plumber'),
    iconfont = require('gulp-iconfont'),
    iconfontcss = require('gulp-iconfont-css'),
    //gfinclude = require('gulp-file-include'),
    spritesmith = require('gulp.spritesmith'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
				pug: 'build/',
        styles: 'build/styles/',
        scripts: 'build/scripts/',
        fonts: 'build/fonts/',
        icons: 'src/style/icons/',
        iconfont: 'src/fonts/comp/',
        pictures: {
            upload: 'build/pictures/upload/',
            sprites: 'build/pictures/sprites/',
            backgrounds: 'build/pictures/backgrounds/'
        }
    },
    src: {
        fonts: 'src/fonts/**/*.*',
        iconfont: 'src/iconfont/**/*.svg',
        img: {
            upload: 'src/img/upload/**/*.*',
            background: 'src/img/background/**/*.*',
            icons: 'src/img/icons/**/*.png'
				},
				js: 'src/js/script.js',
				jslibs: 'src/js/libs/libs.js',
        style: 'src/style/style.scss',
        template: 'src/template',
        html: 'src/*.html',
        pug: 'src/*.pug',
        modules: 'src/modules',
        root: 'src/'
    },
    watch: {
        fonts: 'src/fonts/*.*',
        iconfont: 'src/iconfont/**/*.svg',
        img: ['src/img/**/*.*', '!src/img/icon/'],
        icons: 'src/img/icon/*.*',
				js: ['src/js/**/*.js', 'src/modules/**/*.js'],
        modules: 'src/modules/**/*.*',
        style: ['src/style/**/*.**css', 'src/modules/**/*.**css'],
        templates: 'src/modules/**/*.html',
        html: 'src/*.html',
        pug: ['src/*.pug', 'src/template/*.pug', 'src/modules/*/*.pug']
    },
    clean: 'build/'
};

var config = {
    server: {
        baseDir: "build/"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "front"
};

gulp.task('pug:build', function () {
		return gulp.src(path.src.pug, {base: 'src'})
		.pipe(plumber())
    .pipe(pug({
      pretty: true
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(gulp.dest('build'))
    .pipe(reload({stream: true}));
});

gulp.task('html:build', function () {
		return gulp.src(path.src.html)
		//.pipe(rigger())
    .pipe(gulp.dest('.'))
});

gulp.task('css:build', function () {
		gulp.src(path.src.style)
				.pipe(sourcemaps.init())
				.pipe(sass().on('error', sass.logError)) //Компилируем
        .pipe(prefixer()) // Добавляем вендорные префиксы
				.pipe(cssmin()) // Сжимаем
				.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.styles))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
		gulp.src(path.src.js)
				.pipe(rigger())
				.pipe(sourcemaps.init())
				.pipe(plumber())
				        .pipe(babel({
            presets: [
                ['@babel/preset-env', { "modules": false }]
            ],
            ignore: ['libs.js']
        }))
        .pipe(uglify()) // Сжимаем js
        .pipe(gulp.dest(path.build.scripts))
        .pipe(reload({stream: true}));
});

gulp.task('jslibs:build', function () {
	gulp.src(path.src.jslibs)
			.pipe(rigger())
			.pipe(sourcemaps.init())
			.pipe(uglify()) // Сжимаем js
			.pipe(gulp.dest(path.build.scripts))
			.pipe(reload({stream: true}));
});

gulp.task('image:upload:build', function () {
    gulp.src(path.src.img.upload)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.pictures.upload))
        .pipe(reload({stream: true}));
});

gulp.task('image:background:build', function () {
    gulp.src(path.src.img.background)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.pictures.backgrounds))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

// (Спрайт) Кидаем иконки в папку src/icons - используем .icon-nameicon
gulp.task('iconpictures:build', function () {
    let spriteData =
        gulp.src(path.src.img.icons)
            .pipe(spritesmith({
                imgName: 'icons-sprite.png',
                cssName: 'icons-sprite.scss',
                cssFormat: 'css',
                padding: 1,
                imgPath: `/pictures/sprites/icons-sprite.png`
            }));
    spriteData.img.pipe(gulp.dest(path.build.pictures.sprites));
    spriteData.css.pipe(gulp.dest(path.build.icons));
});

// (Шрифт) Кидаем svg иконки в папку src/iconfont - используем .icon-nameicon
gulp.task('iconfont:build', function () {
    gulp.src([path.src.iconfont])
        .pipe(iconfontcss({
            fontName: 'comp-iconfont',
						targetPath: '../../style/font/comp-iconfont.scss',
            fontPath: './../fonts/comp/',
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            prependUnicode: true,
            normalize: true,
            centerHorizontally: true,
            fontHeight: 1001
        }))
        .pipe(iconfont({
            fontName: 'comp-iconfont',
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            prependUnicode: true,
            normalize: true,
            centerHorizontally: true,
            fontHeight: 1024,
            fontWeight: 1024
        }))
        .pipe(gulp.dest(path.build.iconfont));
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('watch', function () {
    gulp.watch(path.watch.html, ['html:build']);
    gulp.watch(path.watch.modules, ['html:build']);
    gulp.watch(path.watch.templates, ['html:build']);
    gulp.watch(path.watch.pug, ['pug:build']);
		gulp.watch(path.watch.style, ['css:build']);
		gulp.watch(path.watch.script, ['js:build']);
		gulp.watch(path.watch.js, ['js:build']);
		gulp.watch(path.watch.script, ['jslibs:build']);
    gulp.watch(path.watch.js, ['jslibs:build']);
    gulp.watch(path.watch.img, ['image:upload:build', 'image:background:build']);
    gulp.watch(path.watch.iconfont, ['iconfont:build']);
    gulp.watch(path.watch.icons, ['iconpictures:build']);
});

gulp.task('build', [
    'html:build',
    'pug:build',
		'css:build',
		'js:build',
		'jslibs:build',
    'fonts:build',
    'image:upload:build',
		'image:background:build',
		'iconfont:build',
		'iconpictures:build'
]);

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('default', ['build', 'webserver', 'watch']);